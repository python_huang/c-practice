#include <stdio.h>
#include <stdlib.h>

int main()
{
	int c;
	while ((c = getchar()) != EOF) {      
		if (c == '\t')             
			printf("\\t");     
		if (c == '\b')   
			printf("\\b");   
		if (c == '\\')     
			printf("\\\\");  
		else
			putchar(c);
	}
	system("pause");
	return 0;
}
