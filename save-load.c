#include <stdio.h>

void main()
{
	FILE *fin, *fout;
	int n;
	fin = fopen("\in1.txt", "rt");
	if (fin == NULL) {
		printf("Fail To Open File in1.txt!!");
		return;
	}
	fout = fopen("\out1.txt", "w+t");
	if (fout == NULL) {
		printf("Fail To Open File out1.txt!!");
		fclose(fin);
		return;
	}
	fscanf(fin, "%d", &n);
	while (n > 1) {
		fprintf(fout, "%d\n", n);
		if ((n % 2) == 0) n /= 2;
		else n = n * 3 + 1;
	}
	fprintf(fout, "%d\n", n);
	fclose(fin);
	fclose(fout);
}