#include <stdio.h>

void main()
{
	long long int a, b, c;
	while (1) {
		if (scanf("%lld %lld", &a, &b) < 2) 
			break;
		if (a > b) 
			c = a - b;
		else 
			c = b - a;
		printf("%lld\n", c);
	}
}