#include <stdio.h>
#define MAXLEN 1000 //字符串的最大長度
#define LINEWIDTH 20 //行寬 顯示的每行不能超過此寬度
#define MAXBACK 5 //最多回退的字符數 超過則直接斷行 使用中槓結尾
int readline(char str[]);
int doline(char str[], char strnew[]);
int main(void) {
	int len;
	char str[MAXLEN], strnew[MAXLEN];
	//讀入新的字符串 退出輸入按ctrl + d (linux)或ctrl + z (MINGW32)
	while ((len = readline(str)) > 0) {
		//str是原始字符串 strnew是處理後的字符串
		doline(str, strnew);
		//輸出處理後的字符串
		printf("%s", strnew);
	}
	return 0;
}
/*讀入一行輸入的數據*/
int readline(char str[]) {
	int i, c;
	for (i = 0; i < MAXLEN - 1 && (c = getchar()) != EOF && c != '\n'; ++i) {
		str[i] = c;
	}
	if (c == '\n') {
		str[i] = c;
		++i;
	}
	str[i] = '\0';
	return i;
}
/*處理一行輸入的數據*/
int doline(char str[], char strnew[]) {
	int i = 0;//原字符串的計數器
	int j = 0;//新字符串的計數器
	int len = 0;//新行的當前長度
	int c;//原字符串中的字符
	int lastpos = 0;//上一個空格或tab相對於新行行首的位置
	while ((c = str[i]) != '\0') {
		if (len >= LINEWIDTH - 1) {
			//在最後一個字符時會有三種處理情況 每種情況的len和lastpos都有差別 注意查看
			if (c == ' ' || c == '\t') {
				//如果最後一個字符是空格或Tab 則直接換行
				//strnew[j]='A';//調試專用
				strnew[j] = '\n';
				len = -1;//後面要++len 所以此處是-1
				lastpos = 0;//lastpos=len+1
			}
			else if (len - lastpos > 5) {
				//最後一個單詞過長則使用連字符 - 進行連接 而不是直接換行
				//strnew[j+1]='B';//調試專用
				strnew[j] = '-';
				strnew[j + 1] = '\n';
				j += 2;
				strnew[j] = c;
				len = 0;//\n後佔用了一個字符 後面還有++len 所以此處的len是0
				lastpos = 1;//lastpos=len+1
			}
			else {
				//只有當最後一個單詞長度小於5時 才會被放到下一行
				strnew[j] = c;
				//strnew[j-len+lastpos]='C';//調試專用
				strnew[j - len + lastpos] = '\n';
				lastpos = len - lastpos;//將\n之後的字符串算到下一個字符串中
				len = lastpos - 1;//後面有++len 所以此處要-1
			}
			// printf("1j:%d,len:%d,lastpos:%d\n",j,len,lastpos);//調試專用
		}
		else {
			strnew[j] = c;
			if (c == ' ' || c == '\t') {
				lastpos = len;//保存上一個空格或Tab的位置
		// printf("2j:%d,len:%d,lastpos:%d\n",j,len,lastpos);//調試專用
			}
		}
		//三個變量都要自增
		++i;
		++j;
		++len;
	}
	if (c == '\n') {
		strnew[j] = c;
		++j;
	}
	strnew[j] = '\0';
	return 0;
}