#include <stdio.h>
#include <string.h>
#include <stdlib.h>
void input(char s[]);
void expand(char s1[], char s2[]);
int main(){
	char str1[1000], str2[100];
	input(str2);
	expand(str1, str2);
	system("pause");
	return 0;
}
void input(char s[]){
	printf("Please enter string: ");
	gets(s);
}
void expand(char s1[], char s2[]){
	int i, j;
	char t;
	for (i = 0, j = 0; s2[i] != '\0'; i++) {
		if (s2[i] == '-') {
			if (i == 0)
				s1[j++] = '-';
			else if ((s2[i - 1] >= 'a' && s2[i - 1] <= 'z') && (s2[i + 1] >= 'a' && s2[i + 1] <= 'z'))
				for (t = s2[i - 1]; t <= s2[i + 1]; s1[j] = t, j++, t++);
			else if ((s2[i - 1] >= 'A' && s2[i - 1] <= 'Z') && (s2[i + 1] >= 'A' && s2[i + 1] <= 'Z'))
				for (t = s2[i - 1]; t <= s2[i + 1]; s1[j] = t, j++, t++);
			else if ((s2[i - 1] >= '0' && s2[i - 1] <= '9') && (s2[i + 1] >= '0' && s2[i + 1] <= '9'))
				for (t = s2[i - 1]; t <= s2[i + 1]; s1[j] = t, j++, t++);
			else
				s1[j++] = s2[i];
		}
	}
	s1[j] = '\0';
	printf("New line: ");
	puts(s1);
}