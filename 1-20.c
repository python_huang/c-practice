#include <stdio.h>

#define		TABINC		8	//定義每個TABINC(8)個位置會出現一個制表位

int main(){
	int c, nb, pos;
	nb = 0;				//遇到制表符時,到達下個制表位需要的空格數
	pos = 1;			//程序在文本行的當前位置
	while ((c = getchar()) != EOF){
		if (c == '\t'){
			nb 
				= TABINC - (pos - 1) % TABINC;
			while (nb > 0){
				putchar(' ');
				++pos;
				--nb;
			}
		}
		else if (c == '\n'){
			putchar(c);
			pos = 1;
		}
		else{
			putchar(c);
			++pos;
		}
	}
	return 0;
}