﻿#include<stdio.h>
/*題目: 編寫函數strend(s, t)，如果t出現在s的尾部，返回1，否則返回0*/
int strend(char* s, char* t) {
	char* bs = s, * bt = t;
	while (*s++)
		;
	while (*t++)
		;
	while (*--s == *--t) {
		if (bs == s || bt == t)
			break;
	}
	if (bt == t && *s == *t)
		return 1;
	return 0;
}