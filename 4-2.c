﻿#include <stdio.h>
#include <ctype.h>
#define MAXLINE 1000
int getline(char s[], int lim);  //函數原型
double atof(char s[]);
int main(void){
	double val;
	char line[MAXLINE];
	while (getline(line, MAXLINE) > 0)  //科學記數法顯示
		val = atof(line);
	printf("%f\n", val);
	return 0;
}
/*getline函數版本2*/
int getline(char s[], int lim){
	int c, i;
	i = 0;
	while (--lim > 0 && (c = getchar()) != EOF && c != '\n')
		s[i++] = c;
	if (c == '\n')
		s[i++] = c;
	s[i] = '\0';
	return i;
}
/*getline函數版本1*/
/*
int getline(char s[], int lim){
	int c, i;
	for(i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if(c == '\n'){
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}
*/
/*擴展的atof函數*/
double atof(char s[]){
	double val, power;
	int i, sign, n;
	n = 0;
	for (i = 0; isspace(s[i]); ++i)
		;
	sign = (s[i] == '-') ? -1 : 1;
	if (s[i] == '+' || s[i] == '-')
		++i;
	for (val = 0.0; isdigit(s[i]); i++)
		val = 10.0 * val + (s[i] - '0');
	if (s[i] == '.')
		i++;
	for (power = 1.0; isdigit(s[i]); i++){
		val = 10.0 * val + (s[i] - '0');
		power *= 10.0;
	}
	val = sign * val / power;
	if (s[i] == 'e' || s[i] == 'E'){  //擴展atof部分 類似atoi函數
		i++;
		sign = (s[i] == '-') ? -1 : 1;
		if (s[i] == '+' || s[i] == '-')
			i++;
		for (; isdigit(s[i]); ++i){
			n = 10 * n + (s[i] - '0');
		}
		for (; n > 0; --n){  //對上面的val進行提高n個數量級 若n=3 則*1000 若n=-3 則*0.001
			if (sign > 0)
				val = val * 10.0;
			else
				val = val * 0.1;
		}
	}
	return val;
}