#include <stdio.h>
#include <stdlib.h>
#define LOWER 0                                
#define UPPER 300                           
#define STEP 20                               
float trans(int fahr, float celsius);          
int main()
{
	int i;
	float j=0;
	for (i = LOWER; i <= UPPER; i = i + STEP)       
		printf("%3d  %6.1f\n", i, trans(i, j));
	system("pause");
	return 0;
}
float trans(int f, float c)     
{
	c = (5.0 / 9.0)*(f - 32.0);  
	return c;                                   
}