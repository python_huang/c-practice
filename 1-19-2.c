#include <stdio.h>
#include <stdlib.h>
int getline(char s[], int max);
void reverse(char s[], int k);
int main()
{
	char line[1000];
	int len;
	while ((len = getline(line, 1000)) > 0) {
		printf("%s\n", line);
		reverse(line, len);
		printf("%s\n", line);
	}
	system("pause");
	return 0;
}
int getline(char s[], int max)
{
	int i, c;
	for (i = 0; i < max - 1 && (c = getchar()) != EOF && c != '\n'; i++)
		s[i] = c;
	s[i] = '\0';
	return i;
}
void reverse(char s[], int k)
{
	int i, j;
	char temp;
	for (i = 0, j = k - 1; i < (k - 1) / 2; i++, j--) {
		temp = s[i];
		s[i] = s[j];
		s[j] = temp;
	}
	s[k] = '\0';
}