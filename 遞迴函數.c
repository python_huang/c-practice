#include<stdio.h> 

int Fib(int n)
{
	if (n <= 1)
		return 1;
	return Fib(n - 1) + Fib(n - 2);
}
void main(void)
{
	int num;
	printf("請輸入幾個整數:");
	scanf("%d", &num);
	for (int i = 0; i < num; i++)
	{
		printf("Fib(%d)=%d\n", i, Fib(i));
	}
}