#include <stdio.h>

void main(int argc, const char * argv[])
{
	int m, n, c, r; /*c = 雞個數 r = 兔子個數*/

	while (scanf("%d %d", &m, &n) != EOF)
	{
		c = (4 * n - m) / 2;
		r = n - c;

		if (c < 0 || r < 0 || m % 2 == 1)
			printf("error");
		else
			printf("%d %d\n", c, r);
	}
	return 0;
}