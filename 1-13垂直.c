# include <stdio.h>
# define MAX 20
# define MAXLENGTH 20

int main(void) {
	int length[MAX];
	int c;
	int vocl = 0;
	int i;
	int r;

	for (vocl = 0; vocl < MAX; vocl++) {
		length[vocl] = 0;
	}
	vocl = 0;
	while ((c = getchar()) != EOF) {
		if (c >= 'A' && c <= 'Z' || c >= 'a' && c <= 'z' || c == '-') {
			vocl++;
		}
		else {
			length[vocl]++;
			vocl = 0;
		}
	}
	printf("0 1 2 3 4 5 6 7 8 9 10\n");
	for (r = 1; r < MAXLENGTH; r++) {
		for (vocl = 0; vocl < MAX; vocl++) {
			if (r <= length[vocl]) {
				printf("* ");
			}
			else {
				printf("  ");
			}
		}
		putchar('\n');
	}
}