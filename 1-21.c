#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void entab(char s[]);                        //定義函數entab
int main()
{
	char string[80];                         //定義一個足夠大的數組
	printf("Please enter string: ");         //輸入一個字符串
	gets(string);
	entab(string);                           //調用entab函數
	puts(string);                            //輸出變換後的字符串
	system("pause");
	return 0;
}
/*entab函數*/
void entab(char s[])
{
	int i, j, k, len, space_count;                //定義變量 len是字符串長度 space_count是空格計數
	len = strlen(s);                              //測出字符串長度
	s[len] = '\0';                                //給字符串加上終止符
	for (i = 0, space_count = 0; s[i] != '\0'; i++) {
		if (s[i] == ' ') {                        //當判斷出數組中出現空格 開始執行以下條件
			space_count++;                        //空格計數自增加
/*以下7行 是當計數過程中一但滿足空格達到4個的情況 替換為\t
同時將後面字符向前移動 最後使最外面的for循環的變量i的計數位置轉為替換的位置*/
			if (space_count == 4) {
				s[i - 3] = '\\';
				s[i - 2] = '\\t';
				for (j = i - 1, k = i + 1; s[k] != '\0'; s[j++] = s[k++]);
				s[j] = '\0';
				i -= 2;
				space_count = 0;
			}
		}
		else if (space_count > 1) {                 //當判斷數組中不是空格 同時空格計數大於1的情況下開始執行以下條件
			if (space_count == 4) {                 //以下7行的原理同前一個
				s[i - 3] = '\\';
				s[i - 2] = '\\t';
				for (j = i - 1, k = i + 1; s[k] != '\0'; s[j++] = s[k++]);
				s[j] = '\0';
				i -= 2;
				space_count = 0;
			}
			else {                                //此條件為空格技術不是4的情況下的處理
				for (j = i - space_count + 1, k = i; s[k] != '\0'; s[j++] = s[k++]);
				s[j] = '\0';
				i -= 3;
				space_count = 0;
			}
		}
		else                                     //當以上條件都不滿足 意味著空格為1 不需要移動 空格計數歸零
			space_count = 0;
	}
	s[i] = '\0';
}