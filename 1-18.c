#include<stdio.h>
#include<stdlib.h>
#define MAXLINE 1000

int getline(char s[], int lim);
int remove(char s[]);
int main() {
	char line[MAXLINE];
	while (getline(line, MAXLINE) > 0) { //如果存在未處裡的輸入行
		if (remove(line) > 0)
			printf("%s", line);
	}
	system("pause");
	return 0;
}
int getline(char s[], int lim) {
	int i;
	char c;
	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; i++)
		s[i] = c;
	if (c == '\n') {
		s[i] = c;
		i++;
	}
	s[i] = '\0';
	return i;
}
int remove(char s[]) {
	int i = 0;
	while (s[i] != '\n') //每個輸入行末兩位是'\n','\0'
		i++;
	i--;  //back off from '\n'
	//如果當前的字符為' '或者為'\t',則i繼續向前進行判斷
	while (i >= 0 && (s[i] == ' ' || s[i] == '\t')) {
		i--;
	}
	//is it a noblank line?
	if (i >= 0) {  //如果i>=0,則表明輸入行不是空行,至少有一個字符
		i++;
		s[i] = '\n'; //put newline character back
		i++;
		s[i] = '\0'; //terminate the string
	}
	return i;//返回newline的長度
}
