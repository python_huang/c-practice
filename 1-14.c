#include <stdio.h>

int main(void){
	int character[95];
	long count;
	int ch;
	count = 0;
	for (int i = 0; i < 95; i++)
		character[i] &= 0;
	while (EOF != (ch = getchar())){
		++count;
		++character[ch - ' '];
	}
	for (int i = 0; i < 95; i++){
		putchar(i + 32);
		putchar(':');
		for (double c = (double)character[i] / (double)count * 100; 0 < c; --c)
			putchar('|');
		putchar('\n');
	}
	return 0;
}