﻿#include <stdio.h>
#include "calc.h"

char buf = 0;

int getch(void) {
	int c = (buf != 0) ? buf : getchar();
	buf = 0;
	return c;
}

void ungetch(int c) {
	if (buf == 0)
		buf = c;
	else
		printf("ungetch: buf is full\n");
}