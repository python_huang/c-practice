﻿#include <stdio.h>
#include <string.h>
void reverse(char s[]);
void itob(int n, char s[], int b);
void itob(int n, char s[], int b) {//N位一個整數,轉換成B進制的字符串,存入字符串S中
	int i, j, sign;
	if ((sign = n) < 0) n = -n;
	i = 0;
	do {
		j = n % b;//任何一個數的餘數不可能大於他本身
		s[i++] = (j <= 9) ? j + '0' : j + 'a' - 10; //j<=9 用於判定餘數用2,8,16進制表示
	} while ((n = n / b) > 0);                      //10:A 11:B 12:C 13:D 14:E 15:F
	if (sign < 0) s[i++] = '-';
	s[i] = '\0';
	reverse(s);//反向輸出數字
}
void reverse(char s[]) {
	int i = 0;
	int len;
	for (i = (len = strlen(s)); i >= 0; i--) {
		if (s[i] != '\0') putchar(s[i]);
	}
}
int main(){
	int n;
	char s[5];
	int b;
	printf("請輸入要轉換的十進制數字：");
	scanf("%d", &n);
	printf("請輸入要轉換的進制：");
	scanf("%d", &b);
	if (b == 8) printf("0");
	else if (b == 16) printf("0X");
	else if (b == 2) printf("0000");
	itob(n, s, b);
}