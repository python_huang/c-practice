﻿#include <stdio.h>
#define swap(t,x,y) {t temp=x;x=y;y=temp;}
void main()
{
	int a, b;
	float x, y;
	a = 1; b = 2;
	x = 100; y = 200;
	swap(int, a, b);
	swap(float, x, y);
	printf("a=%d,b=%d\n", a, b);
	printf("x=%f,y=%f\n", x, y);
}