#include <stdio.h>
#include <stdlib.h>
double transfer(double f);
int main()
{
	double fahr, cel;
	for (fahr = 0; fahr <= 300; fahr += 20) {
		cel = transfer(fahr);
		printf("%6.2f  %6.2f\n", fahr, cel);
	}
	system("pause");
	return 0;
}
double transfer(double f)
{
	return 5 * (f - 32) / 9;
}