#include <stdio.h>
#define MAXLINE 5

int getLine(char line[], int maxline);
void copy(char to[], char from[]);

int main() {
	int max = 0;
	int len = 0;
	char line[MAXLINE];
	char longestLine[MAXLINE];
	for (int i = 0; i < MAXLINE; i++) {
		line[i] = 0;
		longestLine[i] = 0;
	}
	while ((len = getLine(line, MAXLINE)) > 0) {
		printf("%d, %s", len, line);
		if (len > max) {
			max = len;
			copy(longestLine, line);
		}
		if (max > 0) {
			printf("the longestLine = %s", longestLine);
		}
	}
	return 0;
}
int getLine(char line[], int maxline) {
	int c, i, j;
	c = j = 0;
	for (i = 0; (c = getchar()) != EOF && c != '\n'; i++) {
		if (i < maxline - 2) {
			line[j] = c;
			j++;
		}
	}
	if (c == '\n') {
		line[j] = '\n';
		j++;
		i++;
	}
	line[j] = '\0';
	return i;
}
void copy(char to[], char from[]) {
	int i = 0;
	while ((to[i] = from[i]) != '\0') {
		i++;
	}
}