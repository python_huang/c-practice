﻿/*題目: 給字符串處理函數們加上第三個參數，限制字符數*/
void strncpy(char* s, char* t, int n) {
	while (*t && n-- > 0)
		* s++ = *t++;
	while (n-- > 0)
		* s++ = '\0';
	*s = '\0';
}
void strncat(char* s, char* t, int n) {
	while (*s)
		s++;
	while (*t && n-- > 0)
		* s++ = *t++;
	*s = '\0';
}
int strncmp(char* s, char* t, int n) {
	for (; *s == *t, s++, t++)
		if (*s == '\0' || --n <= 0)
			return 0;
	return *s - *t;
}