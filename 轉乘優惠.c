#include <stdio.h>
#include <math.h>
int main(){
	int mrt = 1, bus = 2, busdiscount = 8;
	int count = 0, discount = 0;
	int i = 5, way, price;
	int countprice[5];
	int	nonprice[5];
	float mrtdiscount = 0.8;
	for (i = 0; i < 5; i++){
		printf("輸入車種和票價");
		scanf("%d %d", &way, &price);
		printf("%d\r\n", i);
		count += price;
		if (5 > i > 0)	{
			if (way == bus) {
				price -= busdiscount;
			}
			if (way == mrt) {
				price = (int)ceil(price * mrtdiscount);
			}
			discount += price;
		}
		nonprice[i] = count;
		countprice[i] = discount;
	}
	printf("結果:\n");
	for (i = 0; i < 5; i++)
		printf("%d %d\n", nonprice[i], countprice[i]);
}