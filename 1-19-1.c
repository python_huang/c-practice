#include <stdio.h>
#include <stdlib.h>
#define MAXLINE 1000
int getline(char line[], int maxline);
void reverse(char to[], char from[]);
int main()
{
	int len;
	char line[MAXLINE];
	char op[MAXLINE];
	while ((len = getline(line, MAXLINE)) > 0)
		reverse(op, line);
	printf("%s", op);
	printf("\n");
	system("pause");
	return 0;
}
int getline(char s[], int lim)
{
	int c, i;
	for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if (c == '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}
void reverse(char to[], char from[])
{
	int i, j, m;
	i = 0;
	while (from[i] != '\0')
		++i;
	m = i - 2;
	j = m;
	for (i = 0; j >= 0; ++i) {
		to[i] = from[j];
		--j;
	}
	to[i] = '\0';
}